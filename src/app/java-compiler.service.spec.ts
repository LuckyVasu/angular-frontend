import { TestBed } from '@angular/core/testing';

import { JavaCompilerService } from './java-compiler.service';

describe('JavaCompilerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: JavaCompilerService = TestBed.get(JavaCompilerService);
    expect(service).toBeTruthy();
  });
});
