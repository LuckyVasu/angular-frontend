import { OnInit } from "@angular/core";
import { Component, ViewChild, AfterViewInit, ElementRef } from "@angular/core";
import * as ace from "ace-builds";
import { JavaCompilerService } from "../java-compiler.service";
import { PythonCompilerService } from "../python-compiler.service";

@Component({
  selector: "app-python-compiler",
  templateUrl: "./python-compiler.component.html",
  styleUrls: ["./python-compiler.component.css"],
})
export class PythonCompilerComponent implements OnInit {
  constructor(private pythonCompilerService: PythonCompilerService) {}

  ngOnInit() {
    ace.config.set("fontSize", "16px");
    ace.config.set(
      "basePath",
      "https://unpkg.com/ace-builds@1.4.12/src-noconflict"
    );
    const aceEditor = ace.edit(this.editor.nativeElement);
    const aceEditor1 = ace.edit(this.editor1.nativeElement);
    let defaultCode = `print("Hello World")`;

    //  aceEditor.session.setValue("<h1>Ace Editor works great in Angular!</h1>");
    aceEditor.setValue("the new text here");
    aceEditor.setValue(defaultCode);
    aceEditor.setTheme("ace/theme/twilight");
    aceEditor.session.setMode("ace/mode/python");
    aceEditor1.session.setValue("OUTPUT:");
    aceEditor1.setReadOnly(true);
    /*aceEditor.on("change", () => {
      console.log(aceEditor.getValue());
    });*/
  }
  @ViewChild("editor", { static: true }) private editor: ElementRef<
    HTMLElement
  >;
  @ViewChild("editor1", { static: true }) private editor1: ElementRef<
    HTMLElement
  >;
  clear() {
    const aceEditor = ace.edit(this.editor.nativeElement);
    aceEditor.setValue(" ");
    const aceEditor1 = ace.edit(this.editor1.nativeElement);
    aceEditor1.setValue("OUTPUT:");
  }

  execrun() {
    const aceEditor = ace.edit(this.editor.nativeElement);
    const formData = new FormData();
    const aceEditor1 = ace.edit(this.editor1.nativeElement);
    console.log(aceEditor.getValue());
    formData.append("content", aceEditor.getValue());
    this.pythonCompilerService.compiler(formData).subscribe(
      (res) => {
        console.log(res);
        aceEditor1.setValue(" OUTPUT    :" + res);
      },
      (err) => {
        console.log(err);
      }
    );
  }
}
