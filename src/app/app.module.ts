import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { MainComponent } from "./main/main.component";
import { AceEditorComponent } from "ng2-ace-editor";
import { JavaCompilerComponent } from "./java-compiler/java-compiler.component";
import { PythonCompilerComponent } from "./python-compiler/python-compiler.component";
import { CppCompilerComponent } from "./cpp-compiler/cpp-compiler.component";
import { HttpClientModule } from "@angular/common/http";
@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    JavaCompilerComponent,
    PythonCompilerComponent,
    CppCompilerComponent,
  ],
  imports: [BrowserModule, AppRoutingModule, HttpClientModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
