import { OnInit } from "@angular/core";
import { Component, ViewChild, AfterViewInit, ElementRef } from "@angular/core";
import * as ace from "ace-builds";
import { JavaCompilerService } from "../java-compiler.service";

@Component({
  selector: "app-java-compiler",
  templateUrl: "./java-compiler.component.html",
  styleUrls: ["./java-compiler.component.css"],
})
export class JavaCompilerComponent implements OnInit {
  constructor(private javaCompilerService: JavaCompilerService) {}
  ngOnInit() {
    ace.config.set("fontSize", "16px");
    ace.config.set(
      "basePath",
      "https://unpkg.com/ace-builds@1.4.12/src-noconflict"
    );
    const aceEditor = ace.edit(this.editor.nativeElement);
    const aceEditor1 = ace.edit(this.editor1.nativeElement);
    let defaultCode = `class Main {
     public static void main(String args[])
     {
      System.out.println("it's working!");
    }
    }`;
    aceEditor1.session.setValue("OUTPUT:");
    aceEditor1.setReadOnly(true);
    //aceEditor.session.setValue("class Example{public static void main(string args[])");

    aceEditor.setValue(defaultCode);

    aceEditor.setTheme("ace/theme/twilight");
    aceEditor.session.setMode("ace/mode/java");
    // aceEditor.on("change", () => {
    //   console.log(aceEditor.getValue());
    // });
  }

  @ViewChild("editor", { static: true }) private editor: ElementRef<
    HTMLElement
  >;
  @ViewChild("editor1", { static: true }) private editor1: ElementRef<
    HTMLElement
  >;

  clear() {
    const aceEditor = ace.edit(this.editor.nativeElement);
    aceEditor.setValue(" ");
    const aceEditor1 = ace.edit(this.editor1.nativeElement);
    aceEditor1.setValue("OUTPUT:");
  }

  // execrun() {
  //   const aceEditor = ace.edit(this.editor.nativeElement);
  //   console.log(aceEditor.getValue());
  // }
  // formdata(it is a string value) is used to get editor value in form object than json, formdata is sent to service method compiler
  exec() {
    const aceEditor = ace.edit(this.editor.nativeElement);
    const formData = new FormData();
    const aceEditor1 = ace.edit(this.editor1.nativeElement);
    formData.append("content", aceEditor.getValue()); // content is the key from springboot
    this.javaCompilerService.compiler(formData).subscribe(
      (res) => {
        console.log(res);
        aceEditor1.setValue(" OUTPUT:" + res);
      },
      (err) => {
        console.log(err);
      }
    );
  }
}
