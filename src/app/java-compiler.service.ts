import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class JavaCompilerService {
  constructor(private http: HttpClient) {}
  //this is sent to backend by using url and string as parameter

  compiler(requestBody) {
    return this.http.post(
      "http://3.236.161.92:6010/compileContent",
      requestBody,
      {
        responseType: "text",
      }
    );
  }
}
