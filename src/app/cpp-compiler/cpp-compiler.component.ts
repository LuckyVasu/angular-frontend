import { OnInit } from "@angular/core";
import { Component, ViewChild, AfterViewInit, ElementRef } from "@angular/core";
import * as ace from "ace-builds";
import { CppCompilerService } from "../cpp-compiler.service";

@Component({
  selector: "app-cpp-compiler",
  templateUrl: "./cpp-compiler.component.html",
  styleUrls: ["./cpp-compiler.component.css"],
})
export class CppCompilerComponent implements OnInit {
  constructor(private cppCompilerService: CppCompilerService) {}

  ngOnInit() {
    ace.config.set("fontSize", "16px");
    ace.config.set(
      "basePath",
      "https://unpkg.com/ace-builds@1.4.12/src-noconflict"
    );
    const aceEditor = ace.edit(this.editor.nativeElement);
    const aceEditor1 = ace.edit(this.editor1.nativeElement);
    let defaultCode = `#include <iostream>
    using namespace std;
      int main() {
      cout << "Hello World!";
      return 0;
    } `;
    aceEditor1.session.setValue("OUTPUT:");
    aceEditor.session.setValue("<h1>Ace Editor works great in Angular!</h1>");
    aceEditor.setValue("the new text here");
    aceEditor.setTheme("ace/theme/twilight");
    aceEditor.session.setMode("ace/mode/gcc");
    aceEditor.setValue(defaultCode);
    aceEditor1.setReadOnly(true);
    /*aceEditor.on("change", () => {
      console.log(aceEditor.getValue());
    });*/
  }
  @ViewChild("editor", { static: true }) private editor: ElementRef<
    HTMLElement
  >;
  @ViewChild("editor1", { static: true }) private editor1: ElementRef<
    HTMLElement
  >;
  clear() {
    const aceEditor = ace.edit(this.editor.nativeElement);
    aceEditor.setValue(" ");
    const aceEditor1 = ace.edit(this.editor1.nativeElement);
    aceEditor1.setValue("OUTPUT:");
  }

  execrun() {
    const aceEditor = ace.edit(this.editor.nativeElement);
    const formData = new FormData();
    const aceEditor1 = ace.edit(this.editor1.nativeElement);
    formData.append("content", aceEditor.getValue()); // content is the key from springboot
    this.cppCompilerService.compiler(formData).subscribe(
      (res) => {
        console.log(res);
        aceEditor1.setValue(" OUTPUT:" + res);
      },
      (err) => {
        console.log(err);
      }
    );
  }
}
